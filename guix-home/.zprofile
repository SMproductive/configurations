export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale
export PATH=$PATH:$HOME/.config/emacs/bin:$HOME/bin:$HOME/.local/bin:$HOME/go/bin:$HOME/.nix-profile/bin:$HOME/.guix-profile/bin
export ZDOTDIR=$HOME/.config/zsh
source $ZDOTDIR/.zshrc
sh ~/configurations/login
